\chapter{Conclusion}
\label{conclusion}

In this thesis, we investigate the use of modularity strategies to make
maintenance of Description Logic ontologies more feasible. More
specifically, we address the complex tasks of ontology repair, including the
choice problem (selecting among multiple plausible repair solutions).

The main contributions consist of a better understanding of the relationship
between modules and ontology repair constructions via the Belief Change
framework, an evaluation of the impact of modularity on the costs of
computing repairs and the design and evaluation of module-based methods to
rank solutions.

As a minor contribution, we highlight the relationship between
Ontology Repair and Belief Change as a continuation of the studies due to 
Matos et al.~\cite{Matos2019}. Furthermore, to aid comprehension,
we employ and extend the new notation to the constructions and operations
defined by Ribeiro~\cite{Ribeiro2013}. The new notation gets rid of cumbersome symbols
for operations and constructions (they are hard to convey and remember) and
generalises the set of unwanted sentences $\Omega$, which
Ribeiro~\cite{Ribeiro2013} fixed beforehand while here it is an argument
of the belief base operations. Consequently, both expansion and contraction
become particular cases of revision, making the presentation of further
results much cleaner.

\section{Summary of the Contributions}

Next, we present a list of the main contributions presented in this study:

\begin{itemize}
    \item Extend the notation for Belief Change operations and constructions
        in DLs defined by Matos et al.~\cite{Matos2019}, which is more explicit and
        convenient than the traditional one.

    \item Show that justification-preserving modules can be adapted to
    preserve MinImps, MaxNons and incision functions.

    \item Display an isomorphism between selection functions for \(\exMaxNons\)
    and those for \(\exMaxNons[\modm]\), where \(\modm = \exbase{\module}\)
    is a JPM for the setting \(\setting\).

    \item Design of a method to compute MinImps while updating a module
    (\cref{alg_mumi}).

    \item Design of algorithms that combine stratification and modularity to
    compute MinImps and MaxNons
    (\cref{alg_mustrat_hst_mi,alg_mustrat_hst_mn}).

    \item Implementation of the algorithms discussed in
        \cref{bbox_bc}, \cref{alg_mumi,alg_mustrat_hst_mi,alg_mustrat_hst_mn,alg_strat_hst_mi,alg_strat_hst_mn}.

    \item Development framework to evaluate algorithms in Belief Change for DL
        ontologies and Ontology Repair. It includes a test case generator
        (OWL2DL-CCC).

    \item Comparison of the different methods to compute MinImps and
        MaxNons, with and without modularity. In summary, modularity
        consistently improves the usage of resources, especially in MinImps
        approaches.

    \item Identify correlations that indicate that modules are more effective
        (proportionally) in the most expensive cases in terms of CPU time
        and memory.

    \item Design of stratification methods based on the atomic
        decomposition (\bah{}, \bad{}, \tah{} and \tad{}).

    \item Implementation of stratification methods (\cispec{}, \axspec{},
        \bah{}, \bad{}, \tah{} and \tad{}). Available as an independent
        library.

    \item Design of metrics to evaluate stratification methods in a general
        setting.

    \item Comparison between \axspec{}, \cispec{}, \bah{}, \bad{}, \tah{}
        and \tad{}, using our metrics for axiom dispersion.

    \item Show that \axspec{} as \bah{} are effective methods to
        mitigate the choice problem in Belief Change of DL bases.
        
\end{itemize}

\section{Discussion}

One of the key topics is to determine how modules can be used to optimise
Belief Change constructions and postulates, and how they affect the
constructions and choice mechanisms involved. In this sense, we have
extended the proposal of Suntisrivaraporn et al.~\cite{Suntisrivaraporn2008}
by adapting their algorithm of MinImps computation with LBMs to MaxNons and
devising a new method of module-based MinImps which updates the module
iteratively. This latter algorithm relies on \cref{star_grow}, which stands
as one of the main theoretical results in this work.

In addition to these algorithms, this study discusses the impact of
modularity on incision and selection functions. In summary, for MinImps, the
incision function is the same before and after modularisation, as the domain
remains the same. For MaxNons, in contrast, the selection function cannot be
the same in general, unless the module is the whole ontology. Still,
\cref{theoretical} identifies a bijection between the MaxNons before and
after modularisation. Thus, to preserve the result, one needs to obtain
a selection function that is isomorphic to the original.
Therefore, the main finding in this aspect is that modularisation via JPMs
does not affect the postulates, and, under some mild conditions, it does not even
change the selection of the outcomes per se.

For the empirical portion of the research, the experiments aim to asses the
impact of one of the most prominent modularity approaches, the syntactic
locality-based modules, on the computational performance of MinImps and
MaxNons algorithms. These algorithms are reasoner-independent
(black-box) and are all based on the hitting set tree algorithm due to
Reiter~\cite{Reiter1987}. This approach was selected not only due to the
popularity but because previous studies have attested its efficiency and
flexibility~\cite{Suntisrivaraporn2008,Horridge2011,Cobe2015}.

A key ingredient was missing for the experiments: a corpus. At the time of
writing of this thesis, there is not a commonly agreed corpus for testing
Belief Change (or Ontology Repair) operations. As discussed in
\cref{framework}, the approaches in the literature in this matter are ad
hoc, with varying designs, generalisation and purpose. These issues
motivated another contribution of this work: a corpus generator for Belief
Change and Ontology Repair.

\Cref{owl2dlccc} introduces OWL2DL-CCC: a prototype tool that takes
as input a set of OWL 2 DL ontologies and a set of error templates in JSON
(with a predetermined structure) and outputs a set of ontologies with the
faults as specified in the templates, together with supporting files.

The corpus for the experiments was built using OWL2DL-CCC, a corpus
of ontologies and a set of templates (used to modify ontologies and
create actual inputs for repair). The ontology dataset consists of snapshot
from the BioPortal curated by Matentzoglu and
Parsia~\cite{Matentzoglu2017a}. The case templates were devised based on
studies found in the literature of Ontology Repair and related
areas~\cite{Horridge2011,Bonatti2015,Cobe2015}.

After elaborating the corpus, the CPU time environments were carefully
prepared to reduce noise when measuring CPU time and maximum memory consumed.
We selected Hermit and JFact figure as reasoners due to compatibility and
ease of integration with the libraries used, in particular OWL API 5.  The
heuristics that implement the shrinking and enlargement phases when
computing MinImps and MaxNons were selected following previous
studies~\cite{Horridge2011,Cobe2015}.  In the experiments discussed in
\cref{empirical}, we measured the number of calls to the reasoner, CPU time
and memory consumed by different strategies to compute MinImps and MaxNons,
some using modularity, while others do not.

With the results of the experiments (discussed in \cref{empirical}), we
attest the benefits of using modules in Belief Base Change (and Ontology
Repair) operations. For MinImps, both when computing the whole set, as when
stopping at the first cut, the savings on all three
quantities evaluated (oracle calls, CPU time and maximum memory consumed)
were evident. For MaxNons, modules were beneficial in most cases when
computing the whole, but only in around half of the cases when computing
a single MaxNon.

Furthermore, in all cases, modules also reduce the percentage of timeouts.
In summary, the verdict is that modules are consistently and significantly
beneficial for computing complete MinImps and MaxNons, and when computing
a cut of all MinImps. However, when computing a single MaxNon, the effect is
not consistently beneficial. Additionally, the results of the correlations indicate
that modules help more in the most resources-consuming instances.
Interestingly, when computing the complete MinImps and MaxNons,
MinImps-computation often requires fewer resources than MaxNons-computation.

For the single solution situation, Basic-MN is generally more
resource-efficient than Basic-MI.\@ In Single-M2, modularity helped
MinImps-based methods much more than it improved MaxNons-based ones. Hence,
when computing the fastest solution with modularity, the difference between
MinImps and MaxNons-based methods disappears: it is not possible to tell
which is better any longer.

Other measurements were also taken into account to explain when modules are
more or less beneficial or in which situations MinImps spends less
resources than MaxNons and vice-versa. However, the only clear association was between the gain with the
addition of modularity and the absolute value of the quantity measured. That
is, more resources consumed (before modularisation) were directly related to
relative improvement when using modules. A proper method to predict when
modules are beneficial is still lacking. 

Regarding the mitigation of the choice problem, the approach proposed is via
stratification methods. Two classification-based strategies were compared
with four modularity-based ones. They were evaluated first in a general
setting, which considers their ability to split the whole ontology,
favouring finer-grained stratifications are better than coarse ones. In this
comparison, BAH has performed better than the others, notably when observing
its IAR distribution.

In the specific case of ontology repair, we put the best
classification-based stratification, \axspec{}, against the best of the
modularity-based, \bah{}. Both methods managed to reduce the number of
plausible outcomes to a far more manageable size.  Still, BAH had fewer
outcomes in general, likely a reflection of its lower IAR distribution,
although the benefits over \axspec{} are only marginal.

\section{Future Works}
\label{futureworks}

On the theoretical side, future works include investigating which modules
are compatible with Pseudo Belief Change, the subarea which investigates
operations that not necessarily satisfy the inclusion postulate. As with the Gentle
Repairs, proposed by Baader et al.~\cite{Baader2018}, the pseudo-operations
allow partial recovery of the information lost when removing
a formula~\cite{Santos2018,Matos2019}. Since inclusion is not required,
pseudo-contraction and pseudo-revision can introduce weakened versions of
the removed axioms, reducing the loss of information.

The theoretical studies we carried out may also have connections with
studies in Defeasible Description
Logics~\cite{Casini2014,Bonatti2015,Britz2019}. For instance, the rational
closure proposed by Casini et al.~\cite{Casini2014}, which relies on
justifications to define a non-monotonic consequence operator. Similarly,
there is the study on the Defeasible Description Logics using the KLM
framework (named after its creators Kraus, Lehmann and
Magidor~\cite{Kraus1990}) for non-monotonic reasoning due to Britz et al.~\cite{Britz2019}.
This study is particularly compelling, given that there are connections
between the paradigm in Belief Change and the KLM paradigm in non-monotonic
reasoning~\cite{Makinson1991,Ribeiro2019}. Therefore, it would be
interesting to investigate the relationship between constructions and
operations we use here and the ones in Defeasible Description Logics.
Moreover, such a relationship may also indicate that modules can be used to
improve performance in this area too.

The main experiment and the focus of \cref{empirical}, which concerns the
optimisation potential of modularity, can also be extended in almost all of its parameters.
First, it can be reproduced with another ontology dataset to observe how
well the results obtained here generalise. Other reasoners, case
specifications, modularity approaches are prime candidates for an extension.
Another extension is to allow other heuristics in the computation of MinImps
and MaxNons to validate the studies from the literature, e.g.\ those due to
Horridge~\cite{Horridge2011} and Cóbe and Wassermann~\cite{Cobe2015}.

A particularly important consideration is that the distribution of module
relative sizes was not uniform. Most inputs had modules sizes with sizes of 20\% or
less of the original ontologies. Moreover, the sizes of $\Omega$ and $\Phi$
could be more varied by using other templates or extending OWL2DL-CCC to produce such instances.

Also, all experiments in \cref{empirical} could be reproduced using $\bot$
and $\top$-LBMs: they are potentially larger but offer less overhead. Also,
modularity approaches such as the inseparability-based modules computed via
datalog reasoning~\cite{Romero2016} (which are smaller than LBMs) can be
used for the same purpose as the LBMs. They enjoy most of the theoretical
results shown in \cref{theoretical} and are at most as large as the LBMs.
Besides, if one ignores the requirements of preserving Belief Change's
postulates such as inclusion, the same studies can also be applied to other
modules with yield justification-preserving sets (see
\cref{justpreserving}), such as the RBMs~\cite{Nortje2013}.

A more controlled corpus of ontologies (possibly, generated
artificially) can aid in determining when modules are more or less
beneficial, and when MinImps fare best against MaxNons methods and
vice-versa. In the other hand, we also need patterns of revision obtained
from practical applications to provide a more accurate account of the impact of
modularity in Ontology Repair in practice.

The next step, considering the results obtained, is to find out the
characteristics that make modularity more or less effective. In this way,
a user or designer can determine beforehand whether to use the repair
techniques as they are or modularise first.  The same investigation can be
carried to decide if a MaxNons-based or a MinImps-based should be used before
starting the repair process. Not only that, but it is also interesting to identify
the cases in which \Cref{alg_mumi}, the module update version of MinImps
computation, dominates over the SME version.

Regarding the stratification methods, future works include the evaluation of
other proposals. A notable one is the idea of syntactic connectivity or
syntactic relevance which figures in Parikh's splitting languages and in
\cref{alg_syncon_mi_enlarge,alg_syncon_mn_shrink}. Moreover, there is no
comparison between stratification methods concerning the resources spent in
their computation. Furthermore, the effect of modularity on the stratified
algorithms in terms of performance (number of reasoner calls and
computational resources) is also left as future work. Another crucial task
is to consider user input to evaluate the stratifications: a good
stratification method should agree (as much as possible) with the users of
the ontology; or at least be deemed useful for those performing the repair.

In terms of Belief Change, it would be relevant to extend the approaches
devised here to other logics, for instance, non-monotonic ones. That would
imply, however, in also extending existing modularity approaches to these
logics.

Another topic left as future work concerns the changes over the modular
structure of an ontology (for instance via its atomic decomposition) after
a repair operation. If we consider the atomic decomposition as a structure
that denotes the importance of axioms, we can also investigate if there is
a relation between the atomic decomposition of the original ontology and the
atomic decomposition of the repaired (or updated) one. There are
preliminary results in this direction by Klinov, Del Vescovo and
Schneider~\cite{Klinov2012}, and more conclusive answers are needed.

As for the artefacts produced, namely: OWL2DL-CCC (the corpus generator),
the ontology stratification library and the DL base change library (which
includes all the code for MinImps and MaxNons computation); all of them have
a wide margin for improvement. For the corpus generator, more concept
constructors (see \cref{tab_sroiq_concepts}) should be included as only
conjunction and disjunction are implemented. The instantiation phase also
needs optimisations and heuristics to filtering entities more efficiently.
Moreover, OWL2DL-CCC can work with inconsistent ontologies only in a very
limited way. Furthermore, the specification format itself can be extended to
provide more flexibility.  Finally, better heuristics can be employed to
optimise the generation of corpora.  The two libraries (stratification and
DL base change) still need refactoring and better integration with other
reasoners outside the OWL API.
