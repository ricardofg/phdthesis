\chapter{Introduction}
\label{ch_intro}

Having clear definitions is perhaps one of the first steps towards devising
reliable, intelligent systems. In this sense, Artificial Intelligence and
Knowledge Representation can provide tools to develop such systems, in
particular via approaches which can formalise these definitions
unambiguously. Studies in these (and other) areas led to the creation of the
ontologies, which are nowadays one of the most
prominent tools in formalising the knowledge of a group about a domain. They
allow users to define concepts and relationships between them in a precise
way that can be shared and understood.

Although ontologies can be defined, and worked with, in a variety of ways
one of the most common definitions states that an ontology is a formal
representation of accorded knowledge about a domain, made to be shared and
also written in a computer accessible language. This definition is an
extension of Gruber's~\cite{Gruber1993}. 

\Cref{moviex_graph} illustrates what type of knowledge ontologies commonly
represent. Each node corresponds to a concept; they can be atomic (drawn with
full lines) or constructed with operators (the circles dashed borders, where
only the operator is highlighted). Each
arrow corresponds to an axiom, a relationship between concepts.  Note that
the graphical notation is used only for informative purposes and it is not
bound to any established standard.

\input{./movieEx_graph.tex}

Ontology repositories such as Ontohub\footnote{\url{https://ontohub.org/}}
and NCBO BioPortal\footnote{\url{https://bioportal.bioontology.org/}}, large
scale ontologies (e.g. SNOMED CT\footnote{\url{http://www.snomed.org/}},
the Gene Ontology (GO)\footnote{\url{http://geneontology.org/}}) and the
growth of the studies in this area attest for their success. Today,
applications using ontology enjoy benefits of representation of concepts
with a formal semantics which facilitates collaboration across different
groups, both in academia and in the industry.

Part of this progress is due to the need for guarantees that several people
are using the same definitions. As remarked by
Matentzoglu~\cite{Matentzoglu2016}, this is particularly true
in areas rich in assertional knowledge such as Biology and Medicine. Another
decisive factor is the creation of a standard language, more specifically
the W3C recommendation of OWL, and more recently OWL 2, as the ontological
languages for their Semantic Web stack.

Large portions of both OWL and OWL 2 are founded on the family of the
Description Logics (DLs), namely the profiles OWL DL and OWL 2 DL,
respectively. Most of these logics are fragments of First Order Logic
(FOL). This allows the development of programs, called reasoners, which
extract implicit information from the axioms expressed in this logic. The
features of this family of logics allow reasoners to be sound (produces only
correct entailments), complete (can compute all entailments) and
terminating procedures.

It is possible to map any DL ontologies, i.e.\ those in OWL DL or OWL 2 DL,
to a set of logical axioms (or formulas) in some DL.\@ This mapping provides
users with the ability to query the ontology for information that, albeit
not expressed explicitly, follows from the formalisation at hand. For
instance, consider the example of \Cref{moviex_graph}, the information that
a $\ocode{ViolentMovie}$ is a $\ocode{Movie}$ is implicit: it is
a consequence of the relationships expressed.

Ontologies have, traditionally, two parts, the TBox which holds terminological
knowledge (the relationships between concepts). The ABox corresponds to
assertional information (information about individuals). \Cref{moviex_graph}
denotes only terminological knowledge (empty ABox). If one includes
information about a specific individual, that is a particular Movie, or
Scene, or any other entity, that knowledge is held in the ABox. Such
differentiation facilitates comprehension, although both parts are mapped to
axioms in DLs. Baader et al.~\cite{Baader2017} illustrate the difference
between TBoxes and ABoxes via an analogy to databases: the TBoxes resemble
database schemas, while ABoxes are similar to database instances.

The ability to operate with tacit knowledge is one of the most powerful
features of this technology. However, this capacity comes with a cost:
deciding entailment in $\mathcal{SROIQ(D)}$, the DL underpinning OWL 2 DL,
is an \textsc{N2ExpTime}-complete problem~\cite{Kazakov2008}. The
high complexity of reasoning can hinder its usage in situations where
real-time answers are required.

Even with this restriction, the field progresses to circumvent this problem.
For instance, there are lightweight DLs, such as $\mathcal{EL++}$, whose
reasoning problem is in \textsc{PTime}~\cite{Baader2005}, and that is
sufficient to express the knowledge in well-known ontologies such as the
Gene Ontology (this holds at least for its 2006 version \cite{Bonatti2015}).
There are many Description Logics other than $\mathcal{EL}$ and
$\mathcal{SROIQ(D)}$, varying in expressivity. Not only that, but they also
differ in the complexity of associated reasoning problems. Furthermore,
reasoners now include a wide range of heuristics and optimisations to avoid
worst-case scenarios and return answers using an acceptable amount of time
and computational resources when possible.

Besides the complexity and costs of reasoning per se, there are still other
obstacles to the adoption of ontologies. One of them is that ontologies
are difficult to maintain, in particular the larger ones. As
a consequence of being able to provide implied relationships between
concepts via logical inference, even a simple modification might introduce
changes unforeseen to the user or designer. Ontology editors such as
Protégé\footnote{\url{https://protege.stanford.edu/}} have some support for
debugging and repairing ontologies (in Protégé's case via the plugins
OntoDebug\footnote{\url{http://isbi.aau.at/ontodebug/}} and
OWLExplanation\footnote{\url{http://owl.cs.manchester.ac.uk/research/explanation/}}),
but there are still shortcomings concerning efficiency and aid in the
decision process.

For instance, adding a new piece of knowledge that contradicts some previous
(explicit or implicit) information may turn the whole ontology
inconsistent. Inconsistencies render all consequences meaningless, as they
make the ontology entail everything. Another common example of faulty
behaviour is incoherence: when one term in the ontology can only assume
empty interpretations.

Inconsistencies and incoherences are not the only types of error in
ontologies. A famous example of faulty behaviour that is neither incoherence
nor inconsistency is that a version of the SNOMED CT entailed that the
amputation of a finger implied the amputation of the whole hand, even
though the formalisation was logically consistent. After identifying an
erroneous consequence like that, pinpointing its causes is an
\textsc{NP}-hard problem even for lightweight
DLs~\cite{Baader2007,Penaloza2009}. Additionally, determining the
problem's source is not enough: there is still a repair to be made.

There are two notable fields in Knowledge Representation whose topics
include debugging and repairing ontologies. One of them is the aptly named
Ontology Repair; the other is called Belief Change. The former studies how
to debug and fix ontologies, usually relying on the logical background
provided by the Description Logics. The latter, Belief
Change~\cite{Ferme2011}, addresses the broader problem of rational change of
beliefs performed by intelligent agents, although more general, it has been
adapted to ontologies by managing them as belief bases of DL
axioms~\cite{Hansson1991,Hansson1994a,Flouris2006,Ribeiro2013}.

The two fields have addressed the same problem with different views.
Ontology Repair being more pragmatic and closer to the ontologies and
Description Logics, while Belief Change solutions tend to use a foundational
approach sustained by logical postulates and characterisations which have
survived the test of time with small modifications.

Research in Ontology Repair often focuses on only one DL (e.g.
$\mathcal{EL}$, $\mathcal{ALC}$) and almost always tries to identify a repair
by looking at the minimal subsets of the ontology that cause the undesired
consequence (known as justifications~\cite{Kalyanpur2006,Horridge2011},
among other names). Moreover, most of these studies search only for maximal
repairs, in a set-inclusion sense. Also, in this area, the methods proposed
often handle TBoxes and ABoxes differently~\cite{Du2015,Benferhat2017}. Many
also use the hierarchy of concepts and other features specific to the
ontologies~\cite{Qi2006}.

Belief Change began with the AGM theory~\cite{Alchourron1985}. Its authors,
Alchourrón, Gärdenfors and Makinson, built a framework consisting of
operations which represent the possible ways an agent might change its
beliefs, the presentation of postulates to rule them, and the mapping of the
postulates to mathematical constructions. Originally, this framework could
only be applied to logics that satisfy the \emph{AGM assumptions} (i.e.\ that
are Tarskian, compact, satisfy the deduction theorem and
supraclassicality)~\cite{Ribeiro2006}. Moreover, in this paradigm, the set
of belief of an agent must be represented by an inference-closed set of
sentences.


Authors such as Hansson, Wassermann, Meyer, Lee, Booth, Flouris and
Ribeiro~\cite{Hansson1991,Hansson2002,Meyer2005,Flouris2006,Ribeiro2013} adapted this
to other formalisms and representations, for instance, sets of DL axioms
which are not (necessarily) closed under consequence, that is, DL
ontologies. Some aspects of the traditional theory remain such as the
definition of operations, postulates and representation theorems to
mathematical constructions. Thus, in Belief Change, ontology repair is
mapped to the problem of changing a set of axioms in some DL according to
one of the operations defined, using only general properties of these logics
such as monotonicity and compactness.

%In this field, the two main constructions are defined with minimal implying
%(faulty) subsets or maximal subsets that are correct. The postulates do not
%determine a single function as possible and the maximality of a repair is
%not a constant requirement.

Both Ontology Repair and Belief Change (for DL bases) have as objective to change a
DL ontology minimally to satisfy some constraints. However, Ontology Repair
focuses on \(\subseteq\)-maximal solutions, while in Belief Change that is
not always the case. We clarity this difference later in
\cref{preliminaries}.

Even with views primarily distinct, similar proposals to ontology
maintenance emerged in both fields. These solutions involve either finding
the minimal subsets responsible by the undesired entailments or finding the
maximal subset of the ontology which does not display the faulty behaviour.
Still, few studies connect the advancements in both fields (notable
exceptions are due to Ribeiro and Wassermann \cite{Ribeiro2009}, and
Benferhat et al.~\cite{Benferhat2017}).

The solutions, however, still suffer from the complexity of the debugging
problem. Both the number of minimal implicants and that of maximally correct
subsets can be exponential in the number of axioms of an
ontology~\cite{Baader2007}. Consequently, there can be an exponential number
of repairs in the number of axioms expressed. Most of the
existing proposals to aid a user in deciding the best outcome, out of the
correct ones, assume that an importance ordering of the axioms
is given without specifying its source or generation.
\todo[inline]{citation
needed}

Therefore, the algorithms for maintenance of these knowledge bases still
need to be improved to allow more systems to enjoy the semantic capabilities
accrued by ontologies. Otherwise, both the cognitive and computational costs
will hamper their usage in applications which would be more reliable and
well-understood. Ontologies and reasoning are particularly interesting in
tasks which need explainable conclusions: the reasoning itself can provide
the logical proofs which underpin the entailments given, also, to allow the
user to query the model represented.

To handle large ontologies, with thousands of axioms (as is the case of
SNOMED CT) it is possible to extract portions of them that are sufficient
for a specific task. These subsets are called modules, and the whole area of
Ontology Modularisation focuses on developing new types of modules and
studying their properties. For instance, for the example of
\Cref{moviex_graph}, a module could omit information about scenes whenever
the application is only interested in trailers.

Given the requirements for faster repair approaches, this thesis
investigates the effectiveness of modularity on addressing the shortcomings
of current approaches to debug and fix ontologies. Namely, these issues
correspond to the high computational costs of repairing ontologies and the
lack of tools aiding the decision process.  As mentioned earlier, modules
are (in a rough sense) subsets of the ontology that suffice for a specific
task. Modularity approaches have been used for reasoning and for
pinpointing the causes of undesired
consequences~\cite{Suntisrivaraporn2008,Horridge2011,Matentzoglu2016}.

There are approaches in the theory of Belief Change that propose similar
improvements as those provided by modularity. Still, the types of modules
suitable for Belief Change and their impact on the theoretical framework are
open questions.

Although the usage of modules in Ontology Repair is not novel, the actual
impact in terms of computational performance is still not well understood.
Most studies restrict themselves to very few ontologies or do not measure
the effect of modularity alone. Also, most of the studies only use
modules with minimal implying subsets. Hence, the impact on approaches which
use the maximally correct subsets is still unknown.

Additionally, both in Belief Change and Ontology Repair, there are methods
to diminish the number of choices that a user has to make among the set of
possible repairs. Often the solution is to assume the existence of partial
order between the axioms and use it to filter the best outcomes. What is
missing from these proposals is how to obtain these orderings.

The objective of this thesis is to provide a clearer understanding of the
impact of modularity in Ontology Repair and Belief Change methods. In
particular, we focus on the uses of modularity to address two problems in
Belief Change and Ontology Repair. The first is the high computational costs
to compute the operations: the \emph{computational problem}. The second is
the decision among exponentially many possible solutions: the \emph{choice
problem}. Here, we extend previous studies in this subject with theoretical
and empirical results to answer three questions concerning these two
problems:

\begin{description}

    \item[Q1] How modules affect the theoretical framework of Belief Change
        for DL bases?

    \item[Q2] How beneficial are modules in improving the performance of repair methods?

    \item[Q3] Is it possible to use modules to mitigate the choice problem?

\end{description}

The questions above are still very general. In \cref{preliminaries}, we
introduce the background knowledge needed to provide more accurate versions
of each question (which we do in \cref{focus}).

%The first part consists of putting modules into the framework
%of Belief Change. The second is to assess the impact of modularity on
%algorithms in both fields using a broader experiment. The third corresponds
%to the proposal and evaluation of modularity-inspired orderings to mitigate
%the choice problem.

There are two main challenges when linking modules and the theory of Belief
Change (more specifically its version dedicated to DL ontologies). One is
the characterisation of suitable approaches, which preserves the postulates
as much as possible. The other is to identify whether the result changes
with modularity, and if so, how to recover the original.

Before proceeding to the theoretical results, some effort is spent on
closing the gap between Ontology Repair and Belief Change. This bridging
attempt aids the presentation of the results to readers familiar with either
field, which is also a minor contribution and continuation of previous work.

As a theoretical contribution, a category of modules that guarantees
seamless integration into the operations defined in Belief Change is
identified, and under mild conditions, the outcome is unaltered after
modularisation. Following the modifications in the theoretical framework, we
describe how to change the repair method to use modules adequately.

Concerning the empirical evaluation of the impact of modularity, the
experimental design aims to broaden the results of existing research and
measure the effect of modules in terms of the number of reasoner calls,
computational time spent and memory consumed (maximum allocated during
execution).

The preparation of the actual corpus for the empirical evaluation also
includes contribution. Currently, there is no corpus for ontology repair and
the solutions provided in the literature are ad hoc. Thus, we developed
a prototype generator which receives two datasets: a set of ontologies and
a set of templates for repair problems. The output is a set of erroneous
ontologies to be repaired (according to the templates) together with
supporting files. 

Besides increasing the datasets used to measure the effects of modules on
repair algorithms, the experiment also varies reasoners and execution
environments. Furthermore, measures were taken to reduce the noise in the
executions, guaranteeing the stability of the results. Based on the
literature, the expectation is that modules have an impact of ``orders of
magnitude''~\cite{Horridge2011} in all quantities considered. The results
show that this expectation is not always fulfilled, but that modules are
still worthwhile, particularly in the most costly situations.

The results also highlight the similarities and differences of performance
between two standard constructions, via minimal implying subsets, and
maximally correct subsets. Moreover, a smaller experiment also attests that
using depth-first search and breadth-first search is not likely to produce
significant changes.

The contributions of this thesis finish with the proposal and evaluation of
the modularity-based stratification methods. These methods aim to mitigate
the choice problem by giving an ordering of the axioms that induces
a ranking on the possible repairs. The results are positive, indicating that
such strategies are competitive considering existing alternatives. 

\section{Publications}

This thesis includes portions of studies presented as papers in
international workshops and in parts of books:

\begin{enumerate}

    \item Ricardo Guimarães and Renata Wassermann. Local change in
    ontologies with atomic decomposition. In Proceedings of the 3rd Joint
    Ontology Workshops (JOWO 2017), Bozen-Bolzano, Italy, September 21-23,
    2017, volume 2050 of CEUR Workshop Proceedings. CEUR-WS.org, 2017. 87,
    92. Preliminary observations that now are part of \cref{modstrat}.

    \item  Ricardo Guimarães, Uli Sattler, and Renata Wassermann. Ontology
    stratification methods: A comparative study. In Proceedings of the 3rd International
    Workshop on Ontology Modularity, Contextuality, and Evolution (WOMoCoE
    2018) co-located with the 16th International Conference on Principles of
    Knowledge Representation and Reasoning (KR 2018), Tempe, Arizona, USA,
    October 29th , 2018., volume 2237 of CEUR Workshop Proceedings, pages
    51–62. CEUR- WS.org, 2018. 87, 98~\cite{Guimaraes2018}. An earlier
    version of~\cref{modstrat}.

    \item Vinícius Bitencourt Matos, Ricardo Ferreira Guimarães, Yuri David
    Santos, and Renata Wassermann. Pseudo-contractions as gentle repairs. In
    Carsten Lutz, Uli Sattler, Cesare Tinelli, Anni-Yasmin Turhan, and Frank
    Wolter, editors, Description Logic, Theory Combination, and All That
    - Essays Dedicated to Franz Baader on the Occasion of His 60th Birthday,
    volume 11560 of Lecture Notes in Computer Science, pages 385–403.
    Springer, 2019~\cite{Matos2019}.
\end{enumerate}

The first and second papers partially compose definitions and results in
\cref{modstrat}. \Cref{bridge} extends on the notation proposed in the third
publication and builds on bringing Ontology Repair and Belief Change
together.


\section{Organisation}

\Cref{preliminaries} contains the preliminaries needed to define the
research problem (and the solutions proposed). In particular, it presents
OWL 2 and Description Logics briefly and gives an overview of the theory of
Belief Change with a focus on its adaptation to DL ontologies and discusses
modularity approaches based on Logic.
This \lcnamecref{preliminaries} also introduces the
modularity techniques that this thesis concentrates on: the syntactic
locality-based modules (LBMs)~\cite{CuencaGrau2008} and the atomic
decomposition (AD)~\cite{DelVescovo2011b}. It also contains a minor
contribution consisting of bridging Ontology Repair and Belief Change
concepts via new notation and secondary observations. 

Next, we discuss the state-of-art of modularity in Ontology
Repair in \cref{focus}. We briefly describe the current panorama and list
the potential benefits and threats to the applicability of modules to address
the computational problem and the choice problem. Additionally, there we refine the research questions that guide the remainder of the study.

\Cref{theoretical,framework,empirical} focus on the effect of modules to
solve the challenge of computational costs. \Cref{theoretical} concentrates most of
the theoretical efforts on linking modules and the Belief Change framework.
While most of the results apply to a broad category of modularisation
approaches, there are remarkable results and algorithms tailored to
syntactic LBMs.

After that comes the empirical part. \Cref{framework} presents the
experimental framework used to assess the impact in practice, including
a description and justification of the datasets and corpus generator,
reasoners and the methodology adopted. Then, \Cref{empirical} discusses the
results obtained with the experiments, assessing the differences in
resources consumed after modularisation. The two standard repair strategies
are compared too. Furthermore, it verifies the hypotheses that explain the
gains via correlation tests.

\Cref{modstrat} is dedicated to the second challenge, that is, the
choice problem. It has both the definitions of the methods and empirical
evaluation, although in a smaller scale than the experiments of
\cref{empirical}.

Finally, \cref{conclusion} contains the summary of contributions, closing
remarks and also highlights the next steps envisioned as a continuation of
this thesis.

\section{Notation}

In this thesis, the symbol $\llang$ represents a generic language.
$\Cn_\llang$ is the consequence operator for that language which is assumed
to be Tarskian (satisfy monotonicity, inclusion and idempotence), compact
and satisfies the deduction theorem. The subscript $\llang$ in $\Cn_\llang$ is
omitted whenever the particular logic $\llang$ is irrelevant or clear from
the context. Also, we often write $\llang$ instead of $(\llang,
\Cn_\llang)$ when referring to a logic to shorten the notation. Lowercase
Greek letters indicate single axioms (or formulas) and uppercase Latin sets
of axioms. For some sets of axioms with special meanings, other symbols will
be used; in particular, calligraphic letters will denote input ontologies
and the symbols $\Omega$ and $\Phi$ will also have a special role (discussed
in \cref{preliminaries}). Also, due to historical reasons, the letter
$\Sigma$ refers to a set of terms (signatures).

If $X$ is a set, $\powerset(X)$ is the power set of $X$ and
$\finitepwset(X) = \{P \in \powerset(X) \mid P \text{ is finite } \}$.
The symbol $\disjunion$ will represent the disjoint union of sets.

Additional notation concerning Belief Change, modules, and Description
Logics and OWL will be introduced in specific sessions in
\cref{preliminaries}. Other symbols will be presented on a case-by-case
basis.



%\todo[inline]{Motivation: Semantic technologies}
%\todo[inline]{Difficulties when updating and fixing ontologies}
%\todo[inline]{Lack of support tools for ontology debug and repair}
