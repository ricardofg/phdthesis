\chapter{Modularity in Ontology Repair}
\label{focus}

In this \lcnamecref{focus}, we discuss the state-of-art concerning the usage
of modules in Ontology Repair, highlights the margins for improvements in
this area and the gaps in the literature concerning the impact of such
techniques. Furthermore, this \lcnamecref{focus} gathers arguments for and
against the usage of modules in Belief Change and Ontology Repair.
Ultimately, the discussion guides the research questions and hypothesis,
which shape our theoretical and empirical pursuits that follow in the next
chapters.

\section{State-of-art}
\label{stateofart}

Currently, justification-preserving modules are already in use for Ontology
Debugging and Repair, especially in MinImps-based
approaches~\cite{Suntisrivaraporn2008,Baader2008,Moodley2010,Nortje2011,Horridge2011}.
These studies show that modularity is indeed effective in saving
computational resources when computing MinImps. The most straightforward and
common approach consists in extracting a JPM for the desired entailment
before computing the ontology's MinImps for
them~\cite{Suntisrivaraporn2008,Baader2008,Horridge2011}. More precisely,
to compute \(\exMinImps[\ontoo][\Omega][\emptyset]\) we can extract \(\modm
= \module(\ontoo, \Omega)\) and then compute \(\exMinImps[\module(\ontoo,
\Omega)][\Omega][\emptyset]\), justification-preservation ensures that
\(\exMinImps[\ontoo][\Omega][\emptyset] = \exMinImps[\module(\ontoo,
\Omega)][\Omega][\emptyset] \).

A finer-grained approach is explored by Moodley~\cite{Moodley2010} that
proposes the extraction of the module at the enlargement phase of the
black-box computation of a MinImp (as in \cref{alg_bb_mi}), this means that
each call to \getNode in \cref{alg_gen_hst} is preceded by a module
extraction that replaces \(\ontoo \setminus \hp\) with \(\module(\ontoo
\setminus \hp, \sig(\Omega \cup \Phi)\) in \cref{call_getnode}.
Nortje~\cite{Nortje2011} instead employs modularity in a glass-box solution.
The technique devised relies on a particular modularity approach that can be
used to obtain MinImps in logics from the $\mathcal{EL}$ family.

Concomitantly to optimisation, there are also modularity-inspired approaches
to solve the choice problem (selecting among exponentially many plausible
repairs). One example is the variant of
\cref{alg_gen_hst} designed by Ji, Qi and Haase~\cite{Ji2009} which can
prioritise the generation of the most important MinImps according to
a criterion inspired by Baader and Suntisrivaraporn's reachability-based
modules~\cite{Baader2008}.

All studies mentioned in this \lcnamecref{stateofart} consider the
computation of MinImps. There is no study on the effects of modularity on
MaxNons-based approaches, both in Belief Change and Ontology Repair areas.

%This scarcity is not surprising given that MinImps are much more
%prevalent in explanations~\cite{Horridge2011} and axiom
%pinpointing~\cite{Baader2007} than MaxNons. The significant difference in the
%number of publications with MinImps-based
%techniques~\cite{Baader2003,Schlobach2003,Schlobach2005,Baader2007,Suntisrivaraporn2008,Baader2008,Ji2009,Horridge2011,Nortje2011}
%versus of those that use MaxNons-based
%ones~\cite{Baader2010,Resina2014,Cobe2015,Ignatiev2017} is evidence of such disparity.

%\todo[inline]{This should be moved to the ``gap'' discussion in \cref{agenda}}


\section{Advantages and Disadvantages of Modularity in Ontology Repair}
\label{arguments}

%\todo[inline]{references or URLs for these ontologies}
As mentioned in \cref{modularity}, modules are being used to improve the
computational performance of algorithms that rely on reasoning
(e.g.\ classification~\cite{Matentzoglu2016} and
explanations~\cite{Horridge2011}). However, few studies quantify the impact
of modularisation in performance. Furthermore, some empirical studies in
this sense do not compare the computation of MinImps with and without
modularity~\cite{Baader2008}, or only evaluate the reduction in the number
of axioms, but do not assess the difference in computational resources
consumed~\cite{Moodley2010,Nortje2011}. A notable exception is due to
Suntisrivaraporn et al.~\cite{Suntisrivaraporn2008} where they compare the
computation of MinImps in large $\mathcal{EL}$ ontologies (namely,
NCI-thesaurus\footnote{\url{https://ncithesaurus.nci.nih.gov/}},
Galen\footnote{\url{http://geneontology.org/}} and
GO\footnote{\url{http://geneontology.org/}}) with and without modularity.
They show that CPU time is improved by orders of magnitude using
$\bot$-locality modules. For MaxNons, to the best of our knowledge, there is
no study which asses the impact of modularity when constructing these sets.

\Cref{argfavor,argagainst} support the decision of using modules to improve
Ontology Repair algorithms and, at the same time, highlight the need for
further studies to quantify their actual influence on performance and
potential interference with choice mechanisms in Belief Change.

\subsection{Benefits}
\label{argfavor}

Aside from the speed up attested by a few studies in the
literature~\cite{Suntisrivaraporn2008}, modules almost always reduce the
search space of reasoning
algorithms~\cite{Baader2008,Moodley2010,Horridge2011}. Tractable approaches
to modularity, such as LBMs, end up including more axioms than
needed~\cite{CuencaGrau2008,Penaloza2017}. Even
so, such techniques are effective in removing axioms that can be safely
ignored and downsizing ontologies to more manageable
sizes~\cite{DelVescovo2012a}.

For repair methods that use black-box strategies, one of the main potential
benefits of reducing the search space is making fewer reasoner calls.  For
instance, in the implementations based on enlargement and shrinking
heuristics (as those introduced in \cref{bbox_bc}) each make fewer
verifications (including entailment tests). In \cref{empirical}, we observe
this effect when computing the complete MinImps and MaxNons.

Furthermore, Matentzoglu~\cite{Matentzoglu2016} carried out a careful
evaluation of the effects of $\bot$-locality-based modules in Ontology
Classification. The study shows that  modules reduce time spent during
reasoning. The main hypotheses is that this gain is either due to test
avoidance (the possibility of doing fewer operations during reasoning) and
``easyfication'' (reasoners managing to make inferences faster).

As for the atomic decomposition, many of its benefits, such as tractability,
stem from LBMs. As discussed in \cref{atomicdecomposition}, the main benefit
of the AD is the ability to represent, in a polynomial way, the dependencies
between all the fundamental (called \emph{genuine}) modules of an ontology.
Moreover, as with LBMs, there is a well-tested implementation available via
the OWL API~\footnotetext{\url{https://owlcs.github.io/owlapi/}}. Above all,
it is so far the best approach for ontology structuring, according to Del
Vescovo~\cite{DelVescovo2013}.

Another fact that is given less attention is that $\bot$ and $\top$-ADs can
establish sequences of interpretations of the terms of the ontology, forming
a chain of conservative extensions. Moreover, both types of AD are closely
related to the class hierarchy of an ontology. \Cref{modstrat} investigates
this relation, extending previous studies~\cite{Guimaraes2018}.

\subsection{Threats} \label{argagainst}

One of the most persuasive arguments against modularity is the overhead
(i.e.\ the additional resources required) in extracting
them~\cite{Matentzoglu2016}. Even polynomial approaches can be
prohibitive for large ontologies. However, as the previous studies indicate,
module extraction's overhead is often compensated by the gains in CPU time for large
ontologies~\cite{Suntisrivaraporn2008,Moodley2010,Horridge2011}.

Another argument against modules is that, in a few situations, modules can
actually make reasoning more expensive, i.e.\ the reasoner consumes more
processing time with a module than with the whole ontology. However, the
study carried by Matentzoglu shows that this only occurs in a tiny
portion of the cases~\cite{Matentzoglu2016} (0.34\% in one approach of
sampling, 0.43\% in the other).

%The last argument against modules that we discusses is restricted to
%MaxNon-based approaches. In the definitions of partial meet operations
%(\cref{def_ipmr, def_epmr, def_pmc}) the selection functions' domains depend
%on the MaxNons, however, if we extract a module

The main issue regarding the atomic decomposition is that it may require one
module extraction for each axiom in the ontology. This process is polynomial
for LBMs, but computationally intensive nonetheless.

In \cref{modstrat}, the AD is used as a basis for stratification methods.
While Guimarães and Wassermann~\cite{Guimaraes2017} proposed the use of the
$\tbs\had$ for similar purposes, prioritising axioms that participate in
more genuine modules over those that participate in fewer. The other two
common ADs, $\top\had$ and $\bot\had$, have meaningful relations with the
class hierarchy and chains of conservative extensions that can expose
importance relations between terms and axioms~\cite{DelVescovo2013}. One
shortcoming of the AD for Ontology Repair is that there is no simple way to
update the structure after a modification in the ontology, incurring further
costs to compute it for the resulting ontology if needed~\cite{Klinov2012}.

Other general problems of the AD hinder its usage, such
as the lack of an efficient way to build arbitrary modules from it. The
approaches that address this problem depend on labels that can be
exponentially large in the length of an axiom, or on hypergraphs
transformations which also display the same exponential issue in the general
case~\cite{Martin-Recuerda2014}.

Another threat to the applicability of modules and the AD is that some
ontologies end up having an atom that includes the vast majority of the
axioms in the ontology. In these cases, the LBMs containing this atom are
big, and the AD is almost useless for distinguishing
axioms~\cite{DelVescovo2013}.

\section{Research Agenda}
\label{agenda}

Among the shortcomings of existing approaches Ontology Repair and Belief
Change for DL bases are the high computational costs associated with
constructing MinImps and MaxNons, and the exponential size of such
sets~\cite{Baader2007,Penaloza2009,Cobe2014}. The
first will be referred as the computational problem, the latter as the
choice problem.

Given the state-of-art and discussion presented in this \lcnamecref{focus},
there are various open questions regarding the use of modules in Ontology
Repair and Belief Change to address the issues mentioned earlier: the
\emph{computional problem} (computing a repair efficiently) and the
\emph{choice problem} (selecting the best repair, among exponentially many).
This study focuses on the following three research questions, which are
refined versions of the ones stated in \cref{ch_intro}

\begin{enumerate}

    \item[Q1] How to integrate modules with the theory of Belief Base
    Change for Description Logics (discussed in \cref{bbcdl})?

    \item[Q2] Can LBMs be used to (consistently) optimise 
        the black-box computation of MinImps and MaxNons (with the
        algorithms shown in \cref{bbox_bc})?  Specifically, concerning
        time, maximum  memory consumed, and the number
        of entailment queries to the reasoner.
        
    \item[Q3] Does the modular structure, via atomic decomposition
        (discussed in \cref{atomicdecomposition}), provides
        a meaningful and effective way to mitigate the choice problem?

\end{enumerate}

The first question involves the identification of sufficient properties that
modules must have to generate operations satisfying the postulates in Belief
Change. If possible, the postulates should remain unchanged, as they provide
a solid basis to describe rational change. This thesis addresses this question
giving special attention to justification-preserving modules, introduced in
\cref{justpreserving}.

More specially, \cref{theoretical} investigates whether JPMs also preserve
MinImps and MaxNons. Also, it analyses effects on incision functions and
selection functions. Finally, it uses preliminary results given in
\cref{preliminaries} to define algorithms that update the initial module
while the sets are computed, providing a better approximation and without
recomputing whole modules from scratch.

Meanwhile, \cref{framework,empirical} are devoted to the second research
question (Q2). This question also relates to the arguments concerning the
overhead of modularity. If the impact proves to be harmful in a significant
amount of cases, then future studies need to be more careful when using
modularity. On the other hand, if modules prove to be advantageous, we will
have a better estimate of their benefits when computing MinImps, and a solid
argument for using them when computing MaxNons. Note that it is even
possible that the impact on the calculus of MinImps and MaxNons differs.

It is also important to note that, in terms of methodology, the experiments
carried out in this thesis extend the ones published by Suntisrivaraporn et
al.~\cite{Suntisrivaraporn2008}. Here, the experiments involve a bigger
corpus of ontologies and evaluate two modularised approaches for computing
MinImps based on \cref{alg_gen_hst} (these approaches will be specified in
\cref{theoretical}). We also include the evaluation for
MaxNons-based methods.

%\todo[inline]{Is it really there that they will be introduced?}

\Cref{framework} presents the experimental framework devised to answer (Q2)
and more specific questions such as the ones we mentioned previously. It
describes not only the implementation but also the design decisions.  In
\cref{empirical}, we detail the experiments constructed to assess the impact
of modularity, discuss the results obtained and the conclusions drawn
from the data gathered.

Next, \cref{modstrat} that address the third research question (Q3). Its
primary approach consists of extending previous studies in this
direction~\cite{Guimaraes2018}. Still, it has novel experiments, results and
discussions. In summary, it proposes stratification methods and compares
then with existing ones in a general setting and in the particular case of
Ontology Repair. We also include measures of correlation between the methods
considered.

In this way, we fill the gaps in knowledge so that researchers and
engineers have more material to decide on the use of modules in these areas.
